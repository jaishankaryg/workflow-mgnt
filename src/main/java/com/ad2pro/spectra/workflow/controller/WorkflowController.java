package com.ad2pro.spectra.workflow.controller;

import com.ad2pro.spectra.workflow.entity.WorkflowConfig;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@Data
@Slf4j
@RestController
@RequestMapping("/workflow/")
public class WorkflowController {

    //workflow configurations
    @RequestMapping(value = "config/add", method = RequestMethod.POST)
    public void addWorkflowConfig(WorkflowConfig workflowConfig){

    }

    @RequestMapping(value = "/config/{id}", method = RequestMethod.GET)
    public void getWorkflowConfig(@PathVariable String id){

    }

    //runtime configurations
    @RequestMapping(value = "nextstates", method = RequestMethod.GET)
    public void getNextStates(Long orderId){

    }

    @RequestMapping(value = "changestate", method = RequestMethod.POST)
    public void changeToNextState(Long orderId){

    }

    @RequestMapping(value = "changestate/{nextState}", method = RequestMethod.POST)
    public void changeState(@PathVariable String nextState){

    }
}
