package com.ad2pro.spectra.workflow.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.util.Date;

@Entity
@Data
public class AuditEntity {

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "date_added", nullable = false)
    private Date dateAdded;

    @Column(name = "date_modified")
    private Date dateModified;

}
