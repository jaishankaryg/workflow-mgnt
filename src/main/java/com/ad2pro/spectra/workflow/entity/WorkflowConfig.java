package com.ad2pro.spectra.workflow.entity;

import com.ad2pro.spectra.workflow.pojo.Transition;
import com.ad2pro.spectra.workflow.pojo.WorkflowUnit;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Entity
@Data
public class WorkflowConfig extends AuditEntity implements Serializable {

    @Column(name = "workflow_name")
    private String workflowName;

    @Column(columnDefinition = "JSON")
    private List<WorkflowUnit> workflowUnits;

    @Column(columnDefinition = "JSON")
    private Map<String, List<Transition>> transitions;

    private String startState;

    private List<String> endStates;

}
