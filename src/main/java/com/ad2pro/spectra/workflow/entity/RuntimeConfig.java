package com.ad2pro.spectra.workflow.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
@Data
public class RuntimeConfig extends AuditEntity {

    @Column(name = "order_id", unique = true)
    Long orderId;

    @Column(name = "workflow_config", columnDefinition = "JSON")
    WorkflowConfig workflowConfig;

    @Column(name = "changed_by")
    String changedBy;

    @Column(name = "current_state")
    String currentState;

}
