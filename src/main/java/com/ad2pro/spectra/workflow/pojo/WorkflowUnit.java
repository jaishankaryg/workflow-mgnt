package com.ad2pro.spectra.workflow.pojo;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Data
public class WorkflowUnit {

    //Type of the workflow unit means, the different types of the workflow units coded in the system. RuntimeInstaces
    //will be created for a given unit based on the type.
    private String workflowUnitType;

    private String name;

    private String statusName;

    private List<String> roles;

    private List<CustomHandler> customHandlers;

}
