package com.ad2pro.spectra.workflow.pojo;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Data
public class Transition {

    String fromName;

    List<String> possibleStates;

}
