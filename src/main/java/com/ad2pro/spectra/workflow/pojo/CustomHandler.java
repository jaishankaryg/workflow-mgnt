package com.ad2pro.spectra.workflow.pojo;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
@Data
public class CustomHandler {

    //CustomEvents can be of type web-notifications, email-notifications etc.
    private String type;

    //customJobs will accept two variables. The variables can have different meanings based on the custom event type
    //Ex: if a web-notification should be sent after 30mins, we can save the values into hashmap as "30", "notification message"
    private Map<String, String> customJobs;

    //Receivers will be the list of emails or the users to be notified
    private List<String> receivers;
}
