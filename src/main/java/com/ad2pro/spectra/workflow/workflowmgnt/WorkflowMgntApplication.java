package com.ad2pro.spectra.workflow.workflowmgnt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WorkflowMgntApplication {

	public static void main(String[] args) {
		SpringApplication.run(WorkflowMgntApplication.class, args);
	}

}
