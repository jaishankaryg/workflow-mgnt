package com.ad2pro.spectra.workflow.exceptions;

public class RuntimeConfigException extends Exception {

    public RuntimeConfigException(String message){
        super(message);
    }
}
