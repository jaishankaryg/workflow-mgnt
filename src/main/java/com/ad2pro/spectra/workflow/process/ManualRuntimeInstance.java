package com.ad2pro.spectra.workflow.process;

import com.ad2pro.spectra.workflow.entity.RuntimeConfig;

public class ManualRuntimeInstance extends RuntimeInstance{


    public ManualRuntimeInstance(RuntimeConfig runtimeConfig) {
        super(runtimeConfig);
    }

    @Override
    public void doValidate(){

    }

    @Override
    public void handleCustomEvent(){

    }
}
