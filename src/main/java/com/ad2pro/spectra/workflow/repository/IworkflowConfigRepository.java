package com.ad2pro.spectra.workflow.repository;

import com.ad2pro.spectra.workflow.entity.WorkflowConfig;
import org.springframework.data.repository.CrudRepository;

public interface IworkflowConfigRepository extends CrudRepository<WorkflowConfig, Long> {
}
