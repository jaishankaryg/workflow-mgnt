package com.ad2pro.spectra.workflow.repository;

import com.ad2pro.spectra.workflow.entity.RuntimeConfig;
import lombok.Data;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IRuntimeConfigRepository extends CrudRepository<RuntimeConfig, Long> {


}
