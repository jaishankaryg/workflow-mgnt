package com.ad2pro.spectra.workflow.service;

import com.ad2pro.spectra.workflow.entity.WorkflowConfig;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Data
@Slf4j
public class WorkflowConfigService {

    public void addWorkflowConfig(WorkflowConfig workflowConfig){

    }

    public WorkflowConfig getWorkflowConfig(Long id){

        return null;
    }

    public WorkflowConfig updateWorkflowConfig(WorkflowConfig workflowConfig, Long orderId){

        return null;
    }
}
