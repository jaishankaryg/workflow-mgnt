package com.ad2pro.spectra.workflow.service;

import com.ad2pro.spectra.workflow.entity.RuntimeConfig;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@Slf4j
@Data
public class RuntimeConfigService {

    public void saveRuntimeConfig(RuntimeConfig runtimeConfig){
        runtimeConfig.setDateAdded(new Date());

    }

    public RuntimeConfig getRuntimeConfig(Long orderId){

        return null;
    }

    public void updateRuntimeConfig(Long runtimeConfigId, RuntimeConfig runtimeConfig){

    }
}


