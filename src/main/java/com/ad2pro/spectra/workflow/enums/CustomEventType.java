package com.ad2pro.spectra.workflow.enums;

public enum  CustomEventType {

    WEB_NOTIFICATION, EMAIL_NOTIFICATION
}
